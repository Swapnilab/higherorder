import "./App.css";
import Shop1View from "./components/Shop1View";
import Shop2View from "./components/Shop2.View";

function App() {
  return (
    <div className="App">
      <Shop1View max={100} />
      <Shop2View max={70} />
    </div>
  );
}

export default App;
