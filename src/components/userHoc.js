import React, { useState } from "react";

const AdvanceShop = (School) => {
  const NewShop = (props) => {
    const [cakeCount, setCakeCount] = useState(10);
    const sellCake = () => {
      setCakeCount((prevState) => prevState - 1);
    };
    const restockCake = (am) => {
      setCakeCount((prevState) => prevState + am);
    };
    console.log(props, "props");
    return (
      <School
        cakeCount={cakeCount}
        restockCake={(am) => restockCake(am)}
        sellCake={sellCake}
        props={props}
      />
    );
  };
  return NewShop;
};

export default AdvanceShop;
