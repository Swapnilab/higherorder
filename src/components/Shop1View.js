import React from "react";
import AdvanceShop from "./userHoc";

const Shop1View = ({ cakeCount, sellCake, restockCake, props }) => {
  return (
    <div>
      Hello 1st Shop You Have {cakeCount} cakes
      <button onClick={sellCake}>Sell</button>
      <button onClick={() => restockCake(5)}>Restock</button>
      You Can Maximum Store {props.max} cake
    </div>
  );
};

export default AdvanceShop(Shop1View);
