import React from "react";
import AdvanceShop from "./userHoc";

const Shop2View = ({ cakeCount, sellCake, restockCake, props }) => {
  return (
    <div>
      Hello 2nd Shop You Have {cakeCount} cakes
      <button onClick={sellCake}>Sell</button>
      <button onClick={() => restockCake(3)}>Restock</button>
      You Can Maximum Store {props.max} cake
    </div>
  );
};

export default AdvanceShop(Shop2View);
